import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './App.css';

export class Funcionarios extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: 0,
      nome: '',
      email: '',
      funcionarios: [],
      modalAberta: false
    };

    this.buscarFuncionarios = this.buscarFuncionarios.bind(this);
    this.buscarFuncionario = this.buscarFuncionario.bind(this);
    this.inserirFuncionario = this.inserirFuncionario.bind(this);
    this.atualizarFuncionario = this.atualizarFuncionario.bind(this);
    this.excluirFuncionario = this.excluirFuncionario.bind(this);
    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);
    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaEmail = this.atualizaEmail.bind(this);
  }

  componentDidMount() {
    this.buscarFuncionarios();
  }

  // GET (todos funcionarios)
  buscarFuncionarios() {
    fetch('http://localhost:60963/api/funcionarios')
      .then(response => response.json())
      .then(data => this.setState({ funcionarios: data }));
  }
  
  //GET (Funcionario com determinado id)
  buscarFuncionario(id) {
    fetch('http://localhost:60963/api/funcionarios/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          email: data.email
        }));
  }

  inserirFuncionario = (funcionario) => {
    fetch('http://localhost:60963/api/funcionarios', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(funcionario)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarfuncionarios();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarFuncionario(funcionario) {
    fetch('http://localhost:60963/api/funcionarios/' + funcionario.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(funcionario)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarFuncionarios();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirFuncionario = (id) => {
    fetch('http://localhost:60963/api/Funcionarios/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarfuncionarios();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarFuncionario(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: "",
      email: "",
      modalAberta: false
    })
  }

  submit = () => {
    const funcionario = {
      id: this.state.id,
      nome: this.state.nome,
      email: this.state.email
    };

    if (this.state.id === 0) {
      this.inserirFuncionario(funcionario);
    } else {
      this.atualizarFuncionario(funcionario);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do funcionario</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <Form.Group>
              <Form.Label>Nome</Form.Label>
              <Form.Control type='text' placeholder='Nome do Funcionario' value={this.state.nome} onChange={this.atualizaNome} />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control type='email' placeholder='Email' value={this.state.email} onChange={this.atualizaEmail} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Nome do funcionario</th>
            <th>E-mail</th>
          </tr>
        </thead>
        <tbody>
          {this.state.funcionarios.map((funcionario) => (
            <tr key={funcionario.id}>
              <td>{funcionario.nome}</td>
              <td>{funcionario.email}</td>
              <td>
                <div>
                  <Button variant="link" onClick={() => this.abrirModalAtualizar(funcionario.id)}>Atualizar</Button>
                  <Button variant="link" onClick={() => this.excluirFuncionario(funcionario.id)}>Excluir</Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <br />
        <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Adicionar Funcionario</Button>
        {this.renderTabela()}
        {this.renderModal()}
      </div>
    );
  }
}