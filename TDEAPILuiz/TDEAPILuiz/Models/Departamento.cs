﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDEAPILuiz.Models
{
    public class Departamento
    {

        public int Id { get; set; }

        public string NomeDepartamento { get; set; }

        public DateTime DataInicio { get; set; }
    }
}
